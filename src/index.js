var path = require('path')
var err = new Error('invalid <package-name>@<semver-version> input')
var scopedPtn = new RegExp(/(^@[^@]*)@(.+)/)
var stdPtn = new RegExp(/(^[^@]*)@(.+)/)

module.exports = function parseNameAtVersion (input) {
  var matcher = input.indexOf('@') === 0 ? scopedPtn : stdPtn
  var match
  if (!input || typeof input !== 'string') throw err
  try {
    match = input.match(matcher)
  } catch (_err) {
    throw err
  }
  return {
    name: match[1],
    version: match[2]
  }
}
