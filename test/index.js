var tape = require('tape')
var parser = require('../')

tape('parses', function (t) {
  var r1 = parser('a@b')
  t.deepEqual(r1, { name: 'a', version: 'b' })
  var r2 = parser('@types/bananas@^1.0.1')
  t.deepEqual(r2, { name: '@types/bananas', version: '^1.0.1' })
  t.end()
})
