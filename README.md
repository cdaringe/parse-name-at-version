[![build status](https://gitlab.com/cdaringe/parse-name-at-version/badges/master/build.svg)](https://gitlab.com/cdaringe/parse-name-at-version/c
ommits/master) ![](https://img.shields.io/badge/standardjs-%E2%9C%93-brightgreen.svg)

# parse-name-at-version

parses `<package-name>@<sem-ver>` strings

## usage

`npm install parse-name-at-version`

## example

```js
var tape = require('tape')
var parser = require('parse-name-at-version')

tape('parses', function (t) {
  t.deepEqual(parser('a@b'), { name: 'a', version: 'b' })
  t.deepEqual(parser('@types/bananas@^1.0.1'), { name: '@types/bananas', version: '^1.0.1' })
  t.end()
})
```

hooray. we support scoped packages.

- throws when parse fails
- does not actually validate `version` is semver compliant
